import Vue from 'vue';
import Router from 'vue-router';
import HomeComponent from '@/components/home.vue';
import Cart from '@/components/cart.vue';
import AddProduct from '@/components/addProduct.vue';
import ProductDetail from '@/components/product_detail';


Vue.use(Router)

export default new Router({
   routes: [
        {
            path:'/',
            name:'Home',
            component:HomeComponent
        },
        {
            path:'/cart',
            name:'Cart',
            component:Cart
        },
        {
            path:'/add_product',
            name:'Add Product',
            component:AddProduct
        },
        {
            path:'/detail/:p_id',
            name:'Product detail',
            component:ProductDetail
        }
    ]
})